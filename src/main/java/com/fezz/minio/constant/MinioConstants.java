package com.fezz.minio.constant;

/**
 * MinIO常量类
 *
 * @author muyun
 * @version 1.0.0
 * @since 2020-07-03
 */
public class MinioConstants {
    public static final String MINIO_CLIENT_PREFIX = "spring.minio";
}
