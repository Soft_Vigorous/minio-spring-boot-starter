package com.fezz.minio;

import com.fezz.minio.constant.MinioConstants;
import com.fezz.minio.properties.MinioProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MinIO auto Configuration
 *
 * @author muyun
 * @version 1.0.0
 * @since 2020-07-03
 */
@Configuration
@ConditionalOnClass(MinioTemplate.class)
@EnableConfigurationProperties(MinioProperties.class)
public class MinioAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = MinioConstants.MINIO_CLIENT_PREFIX, value = "enabled", havingValue = "true")
    MinioTemplate minioTemplate(MinioProperties minioProperties) {
        return new MinioTemplate(minioProperties);
    }
}
