package com.fezz.minio.properties;

import com.fezz.minio.constant.MinioConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * MinIO配置类
 *
 * @author muyun
 * @version 1.0.0
 * @since 2020-07-03
 */
@ConfigurationProperties(prefix = MinioConstants.MINIO_CLIENT_PREFIX)
public class MinioProperties {
    private String endpoint;
    private String accessKey;
    private String secretKey;
    private String bucketName;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
