package com.fezz.minio;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 文件信息
 *
 * @author muyun
 * @version 1.0.0
 * @since 2020-07-03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MinioFile implements Serializable {
    private static final long serialVersionUID = -6701327623785310725L;
    /** 原始文件名 */
    private String originalFileName;
    /** 上传至 minio 的文件名 */
    private String uploadedFileName;
    /** 文件对应 bucketName */
    private String bucketName;
}
